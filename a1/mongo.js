// In myFirstDB, do the following:
/*

1.) Create a "courses" collection and add the following three courses in ONE command:
    -Basic HTML
    -Basic CSS
    -Basic JavaScript

2.) In the same command, make sure to specify the course's description, price, isActive, and add an enrollees array.

For Basic JavaScript ONLY, include an object inside. The object must include a userID field with a value of our firstuser's ObjectID.

*/


//use myFirstDB

//switched to db myFirstDB

db.courses.insertMany([
    {course1: "Basic HTML", price: 7000, description: "Lorem Ipsum", isActive: true},
    {course2: "Basic CSS", price: 8000, description: "Lorem Ipsum", isActive: true},
    {course3: "Basic JavaScript", price: 9000, description: "Lorem Ipsum", isActive: true}
])

//Result
/*
{
        "acknowledged" : true,
        "insertedIds" : [
                ObjectId("61fbd8e6e6a4eac70b30890b"),
                ObjectId("61fbd8e6e6a4eac70b30890c"),
                ObjectId("61fbd8e6e6a4eac70b30890d")
        ]
}
*/

db.courses.updateOne(
    {_id: ObjectId("61fbd8e6e6a4eac70b30890d")},
    {
        $set:{
            enrollees: {

                name: "Jane Doe",
                age: 20,
                isActive: true,
                address: {
                    street: "123 Street St.",
                    city: "New York",
                    country: "United States"
                }
            }
        }
    }
)

//Result
//{ "acknowledged" : true, "matchedCount" : 1, "modifiedCount" : 1 }

//Overall Result

/*db.courses.find().pretty()
{
        "_id" : ObjectId("61fbd8e6e6a4eac70b30890b"),
        "course1" : "Basic HTML",
        "price" : 7000,
        "description" : "Lorem Ipsum",
        "isActive" : true
}
{
        "_id" : ObjectId("61fbd8e6e6a4eac70b30890c"),
        "course2" : "Basic CSS",
        "price" : 8000,
        "description" : "Lorem Ipsum",
        "isActive" : true
}
{
        "_id" : ObjectId("61fbd8e6e6a4eac70b30890d"),
        "course3" : "Basic JavaScript",
        "price" : 9000,
        "description" : "Lorem Ipsum",
        "isActive" : true,
        "enrollees" : {
                "name" : "Jane Doe",
                "age" : 20,
                "isActive" : true,
                "address" : {
                        "street" : "123 Street St.",
                        "city" : "New York",
                        "country" : "United States"
                }
        }
}
*/