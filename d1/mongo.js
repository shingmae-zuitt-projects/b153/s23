/*
    Using MongoDB locally:

    To use MongoDB on your local machine, you must first open a terminal and type the command "mongod" to run the MongoDB Community Server.

    The server must always be running for the duration of your use of your local MongoDB interface/Mongo Shell.

    OPen a separate terminal and type "mongo" to open the Mongo Shell, which is a command line interface that allows us to issue MongoDB commands.

*/

/*

    MongoDB Commands:

    //Shoe list of all databases

    show dbs

    //Create a database

    //use <database name>
    use myFirstDB

    //the use command is only the first step in creating a database. Next, you must create a single record for the database to actually be created

    //Create a single document
    db.users.insertOne({name:"Shing Mae", age:26, isActive: true})


    //Get a list of all users
    db.users.find()

    //Create a second user
    db.users.insertOne({name: "Olib", age: 20, isActive: true})


    //Create a collection
    If a collection does not exist yet, MongoDB creates the collectionwhen you first store data for the collection

    db.products.insertMany([
        {name: "Product 1", price: 200.50, stock: 100, description: "Lorem Ipsum"},
        {name: "Product 2", price: 333, stock: 25, description: "Lorem Ipsum"}
    ])

    //db.products.find()
    //db.products.find({name: "Product 2"})

    //Update an existing document:
    db.users.updateOne(
        {_id: ObjectId("61fbc9a8d11be7e5aa3794be")},        //WHAT to update
        {
            $set: {isActive: false}         //HOW to update it
        }
    )

    db.users.updateOne(
        {_id: ObjectId("61fbccb8bb9a0c0372eff3b0")},
        {
            $set: {name: "Jane Doe"}
        }
    )



    //Update to add a new field:
    db.users.updateOne(
        {_id: ObjectId("61fbc9a8d11be7e5aa3794be")}, 
        {
            $set:{
                address: {
                    street: "123 St.",
                    city: "New York",
                    country: "Unitesd States"
                }
            }
        }
    )

    db.users.updateOne(
        {_id: ObjectId("61fbccb8bb9a0c0372eff3b0")}, 
        {
            $set:{
                address: {
                    street: "123 St.",
                    city: "New York",
                    country: "Unitesd States"
                }
            }
        }
    )



    //Format results in a more presentable
    db.user.find().pretty()

    //Delete a document
        db.users.deleteOne({_id: ObjectId("61fbc9a8d11be7e5aa3794be")})
  
        db.users.deleteOne({_id: ObjectId("61fbcba4bb9a0c0372eff3af")})

*/
